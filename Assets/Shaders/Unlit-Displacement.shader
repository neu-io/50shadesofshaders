// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

// Unlit shader. Simplest possible colored shader.
// - no lighting
// - no lightmap support
// - no texture

Shader "MyShaders/Unlit/Displacement" {
Properties {
    _Color ("Main Color", Color) = (1,1,1,1)
    _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
    _Amount ("Extrusion Amount", Range(-1,1)) = 0.5

}

SubShader {
    Tags { "RenderType"="Opaque" }

    Pass {
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 2.0
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata_t {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
                float3 normal : NORMAL;
               
            };

            struct v2f {
                float4 vertex : SV_POSITION;
                float2 texcoord : TEXCOORD0;
                float3 normal : NORMAL;
            };

            fixed4 _Color;
            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _Amount;

            v2f vert (appdata_t v)
            {
                v2f o;

                /* use macro to transform texture coordinates according to the settings in Material */

                o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);

                // save UVs and modify them by time

                float2 uv = o.texcoord.xy;

                uv.x += _Time.x;

                /* important: for this to work, your model needs normals & uvs */

                /* get the color value of the texture at the vertex  */
                float4 tex = tex2Dlod (_MainTex, float4(uv,0,0));


                /*   move vertex along the normal of the vertex  */
                v.vertex.xyz += v.normal * _Amount * tex;

                o.vertex = UnityObjectToClipPos(v.vertex);

             

                o.normal = v.normal;

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {


                float2 uv = i.texcoord;

                float3 normal = i.normal;
              
                
                fixed4 col = tex2D(_MainTex, i.texcoord);

                col.rgb =  normal * col.rgb;

              
                return col;
            }
        ENDCG
    }
}

}
