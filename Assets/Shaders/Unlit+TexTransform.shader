// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

// Unlit shader. Simplest possible colored shader.
// - no lighting
// - no lightmap support
// - no texture

Shader "MyShaders/Unlit/Color+TexTransform" {
Properties {
    _Color ("Main Color", Color) = (1,1,1,1)
    _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}

}

SubShader {
    Tags { "RenderType"="Opaque" }

    Pass {
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 2.0
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata_t {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;

               
            };

            struct v2f {
                float4 vertex : SV_POSITION;
                float2 texcoord : TEXCOORD0;
            };

            fixed4 _Color;
            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata_t v)
            {
                v2f o;
            
                o.vertex = UnityObjectToClipPos(v.vertex);  // = mul(UNITY_MATRIX_MVP, float4(pos, 1.0))

                // use macro from the UnityCG.cginc include above to make sure texture scale and offset is applied correctly
               
                
                o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
              
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float2 uv = i.texcoord;

              
                
                fixed4 col = float4(1, 0, 0,1) * uv.x;// + float4(0, 1, 0, 1) * uv.y; //   * tex2D(_MainTex, i.texcoord);

                // intrinsic math functions in HLSL: https://docs.microsoft.com/en-us/windows/win32/direct3dhlsl/dx-graphics-hlsl-ddx

                // step, smoothstep, cos, sin, distance, 
                return col;
            }
        ENDCG
    }
}

}
